from django.core.management.utils import get_random_secret_key
from typing import List, Iterable, Any, Union, Optional, Callable
import os
from django.conf import settings

def set_settingsvar_safely(varstring: str, run_if_not_set: Optional[Callable[[Any], Any]]=None, run_if_is_set: Optional[Callable]=None) -> Any:
    """
    Checks Django's settings for a value and runs a function on it, depending on if it has
    already been set.

    If `run_if_not_set` is not passed and the `settings.varstring` is unset then it will return `None`.

    If `run_if_set` is not passed and the settings.`varstring` is set then it will return the value of `settings.varstring`.

    :param varstring: The `settings.varstring` to check for
    :param run_if_not_set: A `Callable` that is run like `run_if_not_set(settings.varstring)`
    :param run_if_is_set: A `Callable` that is run like `run_if_set(settings.varstring)`
    :return: The value of `settings.varstring` after running the `Callable`, or `None`
    """
    val = getattr(settings, varstring, None)
    if val:
        return run_if_is_set(val) if run_if_is_set else val


def return_if_isdir(check_if_isdir: Union[Iterable[str], str], return_instead: Optional[Union[Iterable[Any], str]]=None) -> List:
    if return_instead and check_if_isdir.__class__ is not return_instead.__class__:
        raise TypeError('The arguments `{0}` and `{1}` must be the same type. You\'ve passed `type({0})=={2}` and `type({1})=={3}`.'.format(
            'check_if_isdir', 'return_instead', check_if_isdir.__class__, return_instead.__class__
        ))
    checklist = [check_if_isdir] if isinstance(check_if_isdir, str) else check_if_isdir
    return_instead = [return_instead] if isinstance(return_instead, str) else return_instead if return_instead else checklist
    aredirs = []
    for i, check in enumerate(checklist):
        if os.path.isdir(check):
            aredirs.append(return_instead[i])
    return aredirs


def maybe_regen_secretkey(set_environment_vars: bool=False, write_to_filepath: str=None) -> str:
    """
    Don't regen the key if environment variable SECRET_KEY or DJANGO_SECRET_KEY is set.
    This is mostly untested.
    Inspired by https://gist.github.com/ndarville/3452907#gistcomment-2321128
    """
    secretkey = os.environ.get('SECRET_KEY') or os.environ.get('DJANGO_SECRET_KEY')
    if secretkey is None:
        secretkey = get_random_secret_key()
        if write_to_filepath:
            with open(write_to_filepath, 'w') as secretfile:
                secretfile.write(secretkey)
        if set_environment_vars:
            os.environ.setdefault('SECRET_KEY', secretkey)
            os.environ.setdefault('DJANGO_SECRET_KEY', secretkey)
    return secretkey
